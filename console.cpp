//--------------------------------------------------------------------------
// Module: Console Driver Implementation                    +-------------+
// Author: Michael Nagy                                     | console.cpp |
// Date:   05-May-2010                                      +-------------+
//--------------------------------------------------------------------------

#include "types.h"
#include "console.h"

#include <stdarg.h>
#include <stdio.h>
#include <syslog.h>

#define FMT_SYSLOG(x) va_list args; va_start( args, fmt); vsyslog( x, fmt, args); va_end( args)

void Acc_DbgOutLine( ccptr fmt, ... ) {
  FMT_SYSLOG( LOG_DEBUG);
}

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------


all: scmd

console.o: console.cpp console.h types.h
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c console.cpp -o console.o

util.o: util.cpp util.h serial.h console.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c util.cpp -o util.o

serial.o: serial.cpp serial.h util.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c serial.cpp -o serial.o

modem.o: modem.cpp modem.h serial.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c modem.cpp -o modem.o
	
scmd: scmd.cpp modem.o serial.o util.o console.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) scmd.cpp modem.o serial.o util.o console.o -lrt -lpthread -o scmd

.PHONY: clean
clean:
	-rm -f scmd *.o *~

//--------------------------------------------------------------------------
// Module: Standard Data Types                                  +---------+
// Author: Michael Nagy                                         | types.h |
// Date:   03-Feb-2011                                          +---------+
//--------------------------------------------------------------------------

#include <stdio.h>

typedef unsigned char      byte ;
typedef unsigned short int bit16;
typedef unsigned  long int bit32;
typedef           long int int32;
typedef          char *    cptr ;
typedef const    char *    ccptr;

typedef byte * bptr;
typedef FILE * fptr;

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// Module: Serial Command Utility                              +----------+
// Author: Michael Nagy                                        | scmd.cpp |
// Date:   14-Aug-2012                                         +----------+
//--------------------------------------------------------------------------

// Takes two arguments: a modem command (the AT prefix is implicit) and an
// optional serial port (defaults to /dev/ttyUSB0 if not specified).
//
// Returns the result, or nothing if the string ERROR is returned by the
// serial device.

#include <ctype.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <errno.h>
#include <syslog.h>

#include "types.h"
#include "modem.h"
#include "serial.h"
#include "util.h"
#include "console.h"

int main( int argc, cptr argv[]) {
  openlog(NULL, LOG_CONS, LOG_LOCAL1);
  int ret = 0;
  sem_t * Sem = NULL;
  ccptr UsbPort  = "/var/run/modems/primary/scmd_dev";
  ccptr Command  = "";
  if (argc > 1) Command  = argv[1];
  if (argc > 2) UsbPort  = argv[2];

  ccptr SemName = rindex(UsbPort, '/');
  if (SemName)
    SemName++; /* skip the '/' */
  else
    SemName = UsbPort;

  struct timespec SemTs;
  if (clock_gettime( CLOCK_REALTIME, &SemTs) != 0){
    fprintf( stderr, "*** scmd: error setting timeout.\n. Error: %s", strerror(errno) );
      ret = 4;
  } else {
    SemTs.tv_sec += 10;
    Sem = sem_open( SemName, O_CREAT, S_IRUSR | S_IWUSR, 1);
    if (Sem == SEM_FAILED) {
      fprintf( stderr, "*** scmd: can't open lock '%s' for '%s': %d\n", SemName, UsbPort, errno);
      ret = 2;
    } else {
      if (sem_timedwait( Sem, &SemTs) != 0){
        if (errno == ETIMEDOUT) {
          fprintf( stderr, "*** scmd: timed out waiting for lock on '%s'\n", UsbPort);
          int SemVal;
          sem_getvalue( Sem, &SemVal);
          fprintf( stderr, "*** scmd: Forcing semaphore reset. Value was: %d\n", SemVal);
          sem_unlink(SemName);
          sem_close(Sem);
        } else {
          fprintf( stderr, "*** scmd: could not get lock for '%s'\n", UsbPort);
        }
        ret = 3;
      } else {
        int Modem = 0;
        if (Acc_ModemOpen( Modem, UsbPort)) {
          if (ccptr * Reply = Acc_ModemQueryReply( Modem, Command)) {
            int n = 0;
            while (ccptr x = *Reply++) {      
              printf( "%d:[%s]\n", n++, x);
            }
          } else {
            ret = 4;
          }
          Acc_ModemClose( Modem);
        } else {
          fprintf( stderr, "*** scmd: error opening '%s'\n", UsbPort);
          ret = 1;
      } }
      sem_post( Sem);
  } }
  return ret;
}

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------

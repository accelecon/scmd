//--------------------------------------------------------------------------
// Module: Serial Driver Implementation                      +------------+
// Author: Michael Nagy                                      | serial.cpp |
// Date:   29-Oct-2010                                       +------------+
//--------------------------------------------------------------------------

#include "types.h"
#include "util.h"
#include "serial.h"

#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

bool Acc_SerialOpen( int & PortHandle, ccptr SerialDevice, int OpenOptions) {
  if (PortHandle) {
    printf( "SerialOpenErr 1 [%s]\n", SerialDevice);
    return false;
  }
  PortHandle = open( SerialDevice, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (PortHandle <= 0) {
    PortHandle = 0;
    printf( "SerialOpenErr 2 [%s]\n", SerialDevice);
    return false;
  }
  int BaudRateCode = B115200;
  if (OpenOptions & ACC_SERIALOPEN_9600) {
    BaudRateCode = B9600;
  }
  struct termios DeviceAttributes;
  if (tcgetattr( PortHandle, &DeviceAttributes)) {
    Acc_SerialClose( PortHandle);
    printf( "SerialOpenErr 3 [%s]\n", SerialDevice);
    return false;
  }
  if (OpenOptions & ACC_SERIALOPEN_COOKED) {
    DeviceAttributes.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|INPCK|ISTRIP|INLCR|IGNCR|ICRNL|IXON|IXOFF);
    DeviceAttributes.c_iflag |= IGNPAR; // input modes
    DeviceAttributes.c_oflag &= ~(OPOST|ONLCR|OCRNL|ONOCR|ONLRET|OFILL|OFDEL|NLDLY|CRDLY|TABDLY|BSDLY|VTDLY|FFDLY);
    DeviceAttributes.c_oflag |= NL0|CR0|TAB0|BS0|VT0|FF0; // output modes
    DeviceAttributes.c_cflag &= ~(CSIZE|PARENB|PARODD|HUPCL|CRTSCTS);
    DeviceAttributes.c_cflag |= CREAD|CS8|CSTOPB|CLOCAL; // control modes
    DeviceAttributes.c_lflag &= ~(ISIG|ICANON|IEXTEN|ECHO);
    DeviceAttributes.c_lflag |= NOFLSH; // local modes
  } else {
    DeviceAttributes.c_iflag = 0;
    DeviceAttributes.c_oflag = 0;
    DeviceAttributes.c_cflag = CREAD | CS8 | CLOCAL; // enable receiver, 8n1, no hardware handshaking
    DeviceAttributes.c_lflag = 0;
  }
  cfsetospeed( &DeviceAttributes, BaudRateCode); // output baud rate
  cfsetispeed( &DeviceAttributes, BaudRateCode); //  input baud rate
  if (tcsetattr( PortHandle, TCSANOW, &DeviceAttributes)) {
    Acc_SerialClose( PortHandle);
    printf( "SerialOpenErr 4 [%s]\n", SerialDevice);
    return false;
  }
  return true;
}

bool Acc_SerialGet( int PortHandle, byte & data, int TimeoutMs) {
  if (read( PortHandle, &data, 1) == 1) {
    return true;
  }
  if (TimeoutMs) {
    Acc_UtilSleep_mS( TimeoutMs);
    return Acc_SerialGet( PortHandle, data, 0);
  }
  data = 0;
  return false;
}

bool Acc_NewSerialGet( int PortHandle, byte & data, int TimeoutMs) {
  bool DidRead = false;
  data = '\0';
  while(TimeoutMs && !(read(PortHandle,&data, 1) == 1)) {
    Acc_UtilSleep_mS( 1);
    TimeoutMs--;
  }
  if (data != '\0') {
    DidRead = true;
  }
  return DidRead;
}

bool Acc_SerialPut( int PortHandle, byte data, bool Pace) {
  if (PortHandle > 0) {
    if (Pace) {
      Acc_UtilSleep_mS( 2);
    }
    int rc = write( PortHandle, &data, 1);
    if (rc == 1) {
      return true;
  } }
  return false;
}

bool Acc_SerialPutBuffer( int PortHandle, bptr data, int count, bool Pace) {
  if (PortHandle > 0) {
    if (Pace) {
      for (int i = 0; i < count; i++) {
        if (!Acc_SerialPut( PortHandle, data[i], ACC_SERIALPUT_PACE)) {
          return false;
      } }
      return true;
    } else {
      if (write( PortHandle, data, count) == count) {
        return true;
  } } }
  return false;
}

bool Acc_SerialPutString( int PortHandle, ccptr data, bool Pace) {
  if (PortHandle > 0) {
    int count = strlen( data);
    if (Pace) {
      for (int i = 0; i < count; i++) {
        if (!Acc_SerialPut( PortHandle, data[i], ACC_SERIALPUT_PACE)) {
          return false;
      } }
      return true;
    } else {
      int rc = write( PortHandle, data, count);
      if (rc == count) {
        return true;
  } } }
  return false;
}

bool Acc_SerialClose( int & PortHandle) {
  if (PortHandle > 0) {
    Acc_UtilSleep_mS( 20);
    close( PortHandle);
    PortHandle = 0;
    return true;
  }
  PortHandle = 0;
  return false;
}

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------

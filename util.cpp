//--------------------------------------------------------------------------
// Module: Utility Functions                                   +----------+
// Author: Michael Nagy                                        | util.cpp |
// Date:   07-Aug-2012                                         +----------+
//--------------------------------------------------------------------------

#include "types.h"
#include "util.h"
#include "serial.h"
#include "console.h"

#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/kd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

// Sleep for 1+ seconds.

void Acc_UtilSleep( int s) {
  sleep( s);
}

// Sleep from 0 to 999 milliseconds.

void Acc_UtilSleep_mS( int ms) {
  if (ms > 999) {
    Acc_UtilSleep   ( ms / 1000);
    Acc_UtilSleep_mS( ms % 1000);
  } else {
    if (ms) {
      const timespec ts = { 0, ms * 1000000 };
      nanosleep( &ts, NULL);
} } }

// Sleep from 0 to 999 microseconds.

void Acc_UtilSleep_uS( int us) {
  if (us > 999) {
    Acc_UtilSleep_mS( us / 1000);
    Acc_UtilSleep_uS( us % 1000);
  } else {
    if (us) {
      const timespec ts = { 0, us * 1000 };
      nanosleep( &ts, NULL);
} } }
//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------

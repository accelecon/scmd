//--------------------------------------------------------------------------
// Module: Modem Interface                                    +-----------+
// Author: Michael Nagy                                       | modem.cpp |
// Date:   06-Jun-2011                                        +-----------+
//--------------------------------------------------------------------------

#include "types.h"
#include "util.h"
#include "serial.h"
#include "console.h"
#include "modem.h"

#include <string.h>
#include <termios.h>
#include <stdlib.h>
#include <syslog.h>

// Assemble a response line from the modem.  Lines end with line-feeds.
// Treat all other unprintable characters as blanks.  Never append a
// blank to an empty line or a line that already ends with a blank.
// Also discard trailing blanks.

static ccptr GetLine( int Port, int TimeoutMs = 100) {
  static byte Line[900];
  int n = 0;
  while (n < 118) {
//    if (Acc_SerialGet( Port, Line[n], TimeoutMs)) {
    if (Acc_NewSerialGet( Port, Line[n], TimeoutMs)) {
      if (Line[n] == 0x0a) {
        break; // line-feed ends line
      } else {
        if (!(Line[n] & 0xE0)) {
          Line[n] = ' '; // treat all other unprintables as blanks
        }
        if ((Line[n] != ' ') || (n && Line[n-1] != ' ')) {
          n++; // accept printable character or non-initial isolated blank
      } }
    } else {
      if (n) {
        break; // timeout ends line
      }
      return NULL; // timeout on empty line aborts
  } }
  Line[n] = 0;
  if (n) {
    if (Line[n-1] == ' ') {
      Line[n-1] = 0; // whack any trailing blank
    }
    return strdup( (cptr) Line); // return non-blank line
  }
  return GetLine( Port, TimeoutMs); // ignore blank line and return next line
}

// Send an AT command to the modem (with pacing enabled).

static bool PutLine( int Port, ccptr Command) {
  bool rc = true;
  if (!Acc_SerialPutString( Port, "AT"   , ACC_SERIALPUT_PACE)) rc = false;
  if (!Acc_SerialPutString( Port, Command, ACC_SERIALPUT_PACE)) rc = false;
  if (!Acc_SerialPutString( Port, "\r"   , ACC_SERIALPUT_PACE)) rc = false;
  return rc;
}

// Issue a command to a modem and capture its (possibly mult-line) response.
// Responses should end with OK, ERROR or NO CARRIER.  Only return a captured
// set of response lines if we got an OK.  Use a 300 mS timeout for the
// first returned line only, 100 mS for subsequent lines.

ccptr * Acc_ModemQueryReply( int Port, ccptr Query) {
  tcflush( Port, TCIOFLUSH);
  if (PutLine( Port, Query)) {
    bool Ok = false;
    ccptr *Reply = NULL;
    int Lines = 0, MaxLines = 0;
    while (1) {
	  if (Lines + 1 >= MaxLines) { /* allow for last NULL entry */
	  	MaxLines += 20;
		Reply = (ccptr *) realloc(Reply, MaxLines * sizeof(*Reply));
		if (!Reply)
		  return NULL;
	  	memset(&Reply[MaxLines - 20], 0, 20 * sizeof(*Reply));
	  }
      if ((Reply[Lines] = GetLine( Port, Lines ? 300 : 500))) {
        if (Lines || (strcmp( Reply[Lines], "AT") &&
                      strcmp( Reply[Lines], "OK"))) {
          if (strcmp( Reply[Lines], "OK"                 ) == 0) { Ok = true; break; }
          if (strcmp( Reply[Lines], "ERROR"              ) == 0)              break;
          if (strcmp( Reply[Lines], "NO CARRIER"         ) == 0)              break;
          if (strcmp( Reply[Lines], "COMMAND NOT SUPPORT") == 0)              break;
          Lines++;
        }
      } else {
        break;
    } }
    Acc_DbgOutLine( "  Query: 'AT%s'", Query);
    for (int n = 0; n < Lines && Reply[n]; n++) {
      Acc_DbgOutLine( "    Reply: '%s'", Reply[n]);
    }
    if (!Ok) {
      Acc_DbgOutLine( "Discarding reply.");
    }
    return Ok ? Reply : NULL;
  }
  Acc_DbgOutLine( "PutLine(%d,'%s') failed!", Port, Query);
  return NULL;
}

// Open a modem port.

bool Acc_ModemOpen( int & Handle, ccptr Path) {
  if (Acc_SerialOpen( Handle, Path, ACC_SERIALOPEN_COOKED)) {
    return true;
  }
  Acc_DbgOutLine( "ModemOpen(%s) failed", Path);
  Acc_DbgOutLine( "Error: [ModemOpen(%s) failed]", Path);
  return false;
}

// Close a modem port.

bool Acc_ModemClose( int & Handle) {
  return Acc_SerialClose( Handle);
}

// Utility to safely index into a string array.

static ccptr ModemReplyLine( ccptr * Reply, int Index) {
  if (Reply) {
    for (int i = 0; Reply[i]; i++) {
      if (i == Index) {
        return Reply[i];
  } } }
  return strdup( "OUT-OF-BOUNDS");
}

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------

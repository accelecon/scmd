//--------------------------------------------------------------------------
// Module: Serial Driver Interface                             +----------+
// Author: Michael Nagy                                        | serial.h |
// Date:   29-Oct-2010                                         +----------+
//--------------------------------------------------------------------------

#define ACC_SERIALOPEN_RAW     0x00
#define ACC_SERIALOPEN_COOKED  0x01

#define ACC_SERIALOPEN_115200  0x00
#define ACC_SERIALOPEN_9600    0x02

bool Acc_SerialOpen( int & PortHandle, ccptr SerialDevice, int OpenOptions = 0);

#define ACC_SERIALPUT_PACE false

bool Acc_SerialPut( int PortHandle, byte Data, bool Pace = false);
bool Acc_SerialPutBuffer( int PortHandle, bptr Data, int Count, bool Pace = false);
bool Acc_SerialPutString( int PortHandle, ccptr Data, bool Pace = false);

bool Acc_SerialGet( int PortHandle, byte & Data, int TimeoutMs);
bool Acc_NewSerialGet( int PortHandle, byte & Data, int TimeoutMs);
bool Acc_SerialClose( int & PortHandle);

//--------------------------------------------------------------------------
// End
//--------------------------------------------------------------------------
